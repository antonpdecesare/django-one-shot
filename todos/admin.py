from django.contrib import admin
from todos.models import TodoList, TodoItem


@admin.register(TodoList)
class Todos_listAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "created_on",
    )


@admin.register(TodoItem)
class Todos_itemAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
        "is_completed",
        "list",
    )
