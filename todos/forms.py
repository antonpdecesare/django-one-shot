from django.forms import ModelForm
from todos.models import TodoList


class ListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]
