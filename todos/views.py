from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import ListForm


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "detail": detail,
    }
    return render(request, "todos/details.html", context)


def create_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm()

    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)
